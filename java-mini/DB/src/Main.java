import java.sql.*;

public class Main {

    static Connection c = null;
    static PreparedStatement preparedStatement = null;

    public static void main(String[] args) {

            c = DBUtils.connectDB();

//            fetchData();
            insertData();
            updateData(2,"new name"); //suppose we want to update on student which has the id=2\\
            deleteData(3); //suppose we want to delete student from the table which has id=3\\
    }

    public static void deleteData(int id){
        try {
            preparedStatement = c.prepareStatement("DELETE FROM student WHERE id=?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate(); //use executeUpdate() because we are modifying the data

            System.out.println("Delete successful");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void updateData(int id, String name){
        try {
            preparedStatement = c.prepareStatement("UPDATE student SET name=? WHERE id=?");
            preparedStatement.setString(1, name); //update student's name
            preparedStatement.setInt(2, id);
            preparedStatement.executeUpdate(); //use executeUpdate() because we are modifying the data

            System.out.println("Update successful");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void insertData(){
        try {
            preparedStatement = c.prepareStatement("INSERT INTO student VALUES(?,?,?,?)");
            preparedStatement.setInt(1, 61); //increase id, by yourself
            preparedStatement.setString(2, "Daro");
            preparedStatement.setString(3, "M");
            preparedStatement.setString(4, "PP");

            preparedStatement.executeUpdate(); //use executeUpdate() because we are make some change the data
            System.out.println("Insert successful");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void fetchData() {
        try {
                preparedStatement = c.prepareStatement("SELECT * FROM student WHERE gender = ? AND class= ? ");
                preparedStatement.setString(1, "M");
                preparedStatement.setString(2, "KPS");

                ResultSet rs = preparedStatement.executeQuery(); //use executeQuery() because we are just fetching the data

                while (rs.next()) { // read data row by row, and next() method return true if the resultSet still has next row of data
                    int id = rs.getInt("id");
                    String name = rs.getString("name");
                    String age = rs.getString("gender");
                    String room = rs.getString("class");

                    System.out.println("ID = " + id);
                    System.out.println("NAME = " + name);
                    System.out.println("AGE = " + age);
                    System.out.println("Class = " + room);
                    System.out.println();
                }
                } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}


