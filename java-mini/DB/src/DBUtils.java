import java.sql.*;

public class DBUtils {

    public static Connection connectDB() {
        Connection c = null;

        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                                        "postgres", "12345");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return c;
    }

    public static void closeConnection(ResultSet rs, PreparedStatement preparedStatement, Connection c){
        try {
            rs.close();
            preparedStatement.close();
            c.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
